# export K8SCLUSTER_IMAGE="centos/7"
# export K8SCLUSTER_IMAGE="centos/8"            # KO on k8s-master need review
# export K8SCLUSTER_IMAGE="debian/stretch64"    # Debian 9.12.0
# export K8SCLUSTER_IMAGE="debian/buster64"     # Debian 10.4.0 (VirtualBox 6.1 Only)
# export K8SCLUSTER_IMAGE="bento/ubuntu-16.04"  # Ubuntu 16.04
# export K8SCLUSTER_IMAGE="ubuntu/xenial64"     # Ubuntu 16.04 (VirtualBox 5.2 Only)
# export K8SCLUSTER_IMAGE="ubuntu/bionic64"     # Ubuntu 18.04
# export K8SCLUSTER_IMAGE="ubuntu/focal64"      # Ubuntu 20.04

# Use K8SCLUSTER_IMAGE environment variable to override default image name.
K8SCLUSTER_IMAGE = "#{ENV['K8SCLUSTER_IMAGE']}"
K8SCLUSTER_IMAGE = "centos/7" if K8SCLUSTER_IMAGE.empty?
N = 2

# Link content of ./inventory into .vagrant/provisioners/ansible/inventory to
# reuse group definitions and group_vars.
vagrant_inventory = File.join(File.dirname(__FILE__),
  ".vagrant", "provisioners", "ansible", "inventory")
FileUtils.mkdir_p(vagrant_inventory) if ! File.exist?(vagrant_inventory)
Dir.foreach("inventory") do |item|
  ln_lnk = File.join(vagrant_inventory, item)
  ln_trg = File.join("..", "..", "..", "..", "inventory", item)
  FileUtils.ln_s(ln_trg, ln_lnk) if ! File.exist?(ln_lnk)
end

# Define box default interface
k8scluster_default_ifname = {
  "ubuntu/bionic64" => "enp0s8",
  "ubuntu/focal64" => "enp0s8",
}
k8scluster_default_ifname.default = "eth1"

# Define machine variables
k8scluster_hosts = Array.new
k8scluster_hosts_vars = {}
(0..N).each do |i|
  host_ip = "172.16.50.#{10 + i}"
  k8scluster_hosts[i] = {
    "host_ip" => host_ip,
    "host_name" => "k8s-node-%02d" % i
  }
end
# Apply master specifics
k8scluster_hosts[0]["host_name"] = "k8s-master"

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false

  config.vm.provider "virtualbox" do |v|
    v.memory = 3072
    v.cpus = 2
  end

  (0..N).each do |i|
    host_ip   = k8scluster_hosts[i]["host_ip"]
    host_name = k8scluster_hosts[i]["host_name"]

    config.vm.define host_name do |node|
      node.vm.box = K8SCLUSTER_IMAGE
      node.vm.hostname = host_name
      node.vm.network "private_network", ip: host_ip
      # Override ssh forwarding to allow external access
      node.vm.network "forwarded_port", guest: 22, host: 2200 + i, 
        host_ip: ENV["K8SCLUSTER_SHARE_SSH"] || "127.0.0.1", id: "ssh", auto_correct: true
      k8scluster_hosts_vars[host_name] = {
        "k8scluster_default_ifname" => k8scluster_default_ifname[K8SCLUSTER_IMAGE]
      }
      # Only forward ports on the master
      if i == 0
        node.vm.network "forwarded_port", guest: 6443, host: 6443   # Cluster API HTTPS
        node.vm.network "forwarded_port", guest: 80, host: 8080     # Cluster HTTP
        node.vm.network "forwarded_port", guest: 443, host: 8443    # Cluster HTTPS
        node.vm.network "forwarded_port", guest: 31443, host: 9443  # Dashboard NodePort
      end
      # Only execute once the Ansible provisioner, when all the machines
      # are up and ready.
      # https://www.vagrantup.com/docs/provisioning/ansible#ansible-parallel-execution
      if i == N
        node.vm.provision "ansible" do |ansible|
          ansible.compatibility_mode = "2.0"
          # Disable default limit to connect to all the machines
          ansible.limit = "all,localhost"
          ansible.groups = {}
          ansible.groups["k8s_masters"] = k8scluster_hosts[0, 1].map{|e| e["host_name"]}
          ansible.groups["k8s_nodes"] = k8scluster_hosts[1, N].map{|e| e["host_name"]} if N > 0
          ansible.host_vars = k8scluster_hosts_vars
          ansible.playbook = "playbooks/k8scluster/install.yml"
        end
      end
    end
  end
end
