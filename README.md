# Ansible Collection - stago.k8scluster

## Presentation

This collection is a simple showcase of a lightweight Kubernetes deployment, mainly the extension of [geerlingguy / ansible-role-kubernetes](https://github.com/geerlingguy/ansible-role-kubernetes).

Deployment is validated by [Molecule CI/CD](.gitlab-ci.yml) on following platforms:

    - CentOS 7
    - Debian 9 (Stretch)
    - Debian 10 (Buster)
    - Ubuntu 16.04 LTS (Xenial Xerus)
    - Ubuntu 18.04 LTS (Bionic Beaver)
    - Ubuntu 20.04 LTS (Focal Fossa)

This sample shows how to:

- Use [Ansible](https://docs.ansible.com/) to deploy your [kubenetes](https://kubernetes.io/) cluster with kubernetes applications.
- Use [Ansible inventory override](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#using-multiple-inventory-sources).
- Use [Ansible Molecule](https://molecule.readthedocs.io/en/latest/) in CI/CD to validate collection deployment.
- Use [Vagrant](https://www.vagrantup.com/) to create Ansible development / preproduction environment.
- Derive and synchronize opensource upstreams with git submodules.

## System requirements

- Python >=3.5
- Ansible 2.10

Linux is definitively the preferred platform to develop and use this collection.

## How to install

### 1 - Install system dependencies

```bash
sudo apt install python3-pip python3-venv
```

### 2 - Get the sources

```bash
# For users who have registered their ssh key in the Gitlab server:
git clone --recursive git@gitlab.com:bboss-org/kube/k8scluster.git

# For others:
git clone --recursive https://gitlab.com/bboss-org/kube/k8scluster.git
```

### 3 - Create python virtual environment

```bash
python3 -m venv ~/.k8scluster/venv --prompt k8scluster
source ~/.k8scluster/venv/bin/activate
pip install --upgrade pip wheel setuptools
pip install -r k8scluster/requirements.txt
```

### 4 - Install Ansible requirements

```bash
ansible-galaxy install -r k8scluster/requirements.yml
```

## How to start

```bash
source ~/.k8scluster/venv/bin/activate

# Pre-production deployment (Vagrant + VirtualBox)
cd k8scluster
vagrant up

# Production deployment
cd k8scluster
ansible-playbook -i inventory -i ../your_inventory playbooks/install.yml 
```

For further information on Pre-production deployment, please read
[vagrant_cluster.md](docs/vagrant_cluster.md)

## How to connect to applications

All applications are available by their FQDN by default (ingress rule).

| Application    | FQDN                                         | Sample                                   |
|----------------|----------------------------------------------|------------------------------------------|
| kube-apiserver | `"{{ k8scluster_network_fqdn }}"`            | `https://k8scluster.nuci7.lan:6443`      |
| dashboard      | `"dashboard.{{ k8scluster_network_fqdn }}"`  | `https://dashboard.k8scluster.nuci7.lan` | 
| grafana        | `"grafana.{{ k8scluster_network_fqdn }}"`    | `http://grafana.k8scluster.nuci7.lan`    |
| nexus          | `"nexus.{{ k8scluster_network_fqdn }}"`      | `http://nexus.k8scluster.nuci7.lan`      |
| prometheus     | `"prometheus.{{ k8scluster_network_fqdn }}"` | `http://prometheus.k8scluster.nuci7.lan` |

`k8scluster_network_fqdn` can be customized into your specific inventory or with the `K8SCLUSTER_FQDN` environment variable.

As the **kube-apiserver** is available via ingress, [Lens](https://k8slens.dev/) can be used to manage the cluster.

## Environment variables

| Variable               | Default value             | Sample value                         | Comment                     |
|------------------------|---------------------------|--------------------------------------|-----------------------------|
| `K8SCLUSTER_FQDN`      | `{{ ansible_fqdn }}`      | `k8s-master`                         | Master host FQDN by default |
| `K8SCLUSTER_DOCKERHUB` | `https://index.docker.io` | `http://dockerhub.bbossci.nuci3.lan` | Docker proxy                |

## Credits

This collection contains roles created by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/).

- [roles.3rd_party/contrib_docker](https://github.com/geerlingguy/ansible-role-docker.git)
- [roles.3rd_party/contrib_helm](https://github.com/geerlingguy/ansible-role-helm.git)
- [roles.3rd_party/contrib_kubernetes](https://github.com/geerlingguy/ansible-role-kubernetes.git)
- [roles.3rd_party/contrib_nfs](https://github.com/geerlingguy/ansible-role-nfs.git)

This collection contains a roles created by [Petr Ruzicka](https://petr.ruzicka.dev/)
- [roles.3rd_party/contrib_proxy_settings](https://github.com/ruzickap/ansible-role-proxy_settings)

This collection contains a roles created by [Simon Bärlocher](https://sbaerlocher.ch/)
- [roles.3rd_party/contrib_ca_certificates](https://github.com/arillso/ansible.ca-certificates)

Roles have been attached as submodules to keep track of changes,control non-regression and to show how to derive a role without modifying the upstream.