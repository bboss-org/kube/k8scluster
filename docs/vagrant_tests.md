Configurations Dashboard
-----------------------------------------------------------

| Hosts      | IP Address   |
|------------|--------------|
| k8scluster | 10.0.2.15    |
| k8s-master | 172.16.50.10 |
| k8s-node-1 | 172.16.50.11 |
| k8s-node-1 | 172.16.50.12 |

externalIPs
-----------------------------------------------------------
```yaml
spec:
  clusterIP: 10.233.53.152
  externalIPs:
  - 10.0.2.15
  ports:
  - port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```
```bash
# Tests OK sur tous les nodes (1-3):
curl --insecure https://10.0.2.15:443

# Test KO sur tous les nodes (1-3):
curl --insecure https://172.16.50.10:443
curl --insecure https://172.16.50.11:443
curl --insecure https://172.16.50.12:443
```

NodePort
-----------------------------------------------------------
```yaml
spec:
  clusterIP: 10.233.53.152
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 31443
    port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```
```bash
# Tests OK sur tous les nodes (1-3):
curl --insecure https://10.0.2.15:31443
curl --insecure https://172.16.50.10:31443
curl --insecure https://172.16.50.11:31443
curl --insecure https://172.16.50.12:31443

# Test KO sur tous les nodes (1-3):
curl --insecure https://10.0.2.15:443
curl --insecure https://172.16.50.10:443
curl --insecure https://172.16.50.11:443
curl --insecure https://172.16.50.12:443
```

externalIPs + NodePort
-----------------------------------------------------------
```yaml
spec:
  clusterIP: 10.233.53.152
  externalIPs:
  - 10.0.2.15
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 31443
    port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```
```bash
# Tests OK sur tous les nodes (1-3):
curl --insecure https://10.0.2.15:443
curl --insecure https://10.0.2.15:31443
curl --insecure https://172.16.50.10:31443
curl --insecure https://172.16.50.11:31443
curl --insecure https://172.16.50.12:31443

# Test KO sur tous les nodes (1-3):
curl --insecure https://172.16.50.10:443
curl --insecure https://172.16.50.11:443
curl --insecure https://172.16.50.12:443
```
