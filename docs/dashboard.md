# Kubernetes Dashboard

## Deployment
Kubernetes Dashboard deployments manifests are stored into the playbooks/dashboard/templates/\<version\>/ directory:
- ``*.yml.ori`` are the original version uploaded from internet.
- ``*.yml.j2`` are the jinja version used by ansible with parameter replacement or not.

Both files are kept in the repository to be able to identify and reapply modifications made for templating.<br>
*Original files are renamed \*.ori to skip ymallint check and avoid unjustified errors with this project's coding rules.*

## Kubectl commands processed by ansible
```bash
kubectl apply -f ~/.k8scluster/dashboard/deploy.yml
kubectl apply -f ~/.k8scluster/dashboard/account.yml
kubectl apply -f ~/.k8scluster/dashboard/ingress.yml
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

## Network configuration
Dashboard is accessible in https on a NodePort
```text
kubectl get service -n kubernetes-dashboard

NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)         AGE
dashboard-metrics-scraper   ClusterIP   10.101.121.107   <none>        8000/TCP        10m
kubernetes-dashboard        NodePort    10.102.1.49      <none>        443:31443/TCP   10m
```
``dashboard_nodeport`` contains the port number and the value can be overriden into your own inventory.
```yaml
# inventory/groupvars/k8s_masters.yml

dashboard_nodeport: 31443
```
However, when you run the ansible playbook from vagrant provisionning, whatever the content of your inventory: 
- Vagrant forces the port to the default value (``31443``).<br>
- Map the NodePort to a host port (``9443``) for external access.
```text
vagrant port k8s-master

The forwarded ports for the machine are listed below. Please note that
these values may differ from values configured in the Vagrantfile if the
provider supports automatic port collision detection and resolution.

    22 (guest) => 2200 (host)
 31443 (guest) => 9443 (host)
 ```

## Network Tests on Vagrant cluster
Commands to test the dashboard from the k8s-master vm:
```bash
vagrant ssh k8s-master
# ClusterIP address (calico)
curl --insecure https://192.168.235.192:31443
# Nodes address
curl --insecure https://172.16.50.10:31443
curl --insecure https://172.16.50.11:31443
curl --insecure https://172.16.50.12:31443
```
Commands to test the dashboard from the host:
```bash
# Vagrant nat
curl --insecure https://127.0.0.1:9443
# Vagrant private Network
curl --insecure https://172.16.50.10:31443
curl --insecure https://172.16.50.11:31443
curl --insecure https://172.16.50.12:31443
```

## Troubleshoot Dashboard
```bash
# Check pod is started
kubectl -n kubernetes-dashboard get pods

NAME                                         READY   STATUS             RESTARTS   AGE
dashboard-metrics-scraper-7b59f7d4df-n8ldc   1/1     Running            0          30m
kubernetes-dashboard-5dbf55bd9d-hl9t2        0/1     CrashLoopBackOff   9          30m

# Check pod events
# kubectl -n kubernetes-dashboard describe pods kubernetes-dashboard- [-> Tab key]
kubectl -n kubernetes-dashboard describe pods kubernetes-dashboard-5dbf55bd9d-hl9t2

Events:
  Type     Reason     Age                    From               Message
  ----     ------     ----                   ----               -------
  Normal   Scheduled  32m                    default-scheduler  Successfully assigned kubernetes-dashboard/kubernetes-dashboard-5dbf55bd9d-hl9t2 to k8s-node-1
  Normal   Pulled     32m                    kubelet            Successfully pulled image "kubernetesui/dashboard:v2.0.3" in 12.040071669s
  ...
  Warning  BackOff    2m44s (x120 over 31m)  kubelet            Back-off restarting failed container

# Check pod logs
# kubectl -n kubernetes-dashboard logs --previous kubernetes-dashboard [-> Tab key]
kubectl -n kubernetes-dashboard logs --previous kubernetes-dashboard-5dbf55bd9d-hl9t2
```

## Update Dashboard templates
1. To add a new dashboard version, download the deployment file directly into the template/dashboard directory.
```bash
export DASHBOARD_VERSION_OLD="v2.0.3"
export DASHBOARD_VERSION_NEW="v2.0.4"
cd ./roles/kubernetes/templates/dashboard/
mkdir ${DASHBOARD_VERSION_NEW}
wget https://raw.githubusercontent.com/kubernetes/dashboard/${DASHBOARD_VERSION_NEW}/aio/deploy/recommended.yaml -O ${DASHBOARD_VERSION_NEW}/deploy.yml.ori
cp ${DASHBOARD_VERSION_NEW}/deploy.yml.ori ${DASHBOARD_VERSION_NEW}/deploy.yml.j2
```
2. Apply jinja parameters used for the previous version.
```bash
diff -u ${DASHBOARD_VERSION_OLD}/deploy.yml.ori ${DASHBOARD_VERSION_OLD}/deploy.yml.j2 > deploy.yml.diff
patch ${DASHBOARD_VERSION_NEW}/deploy.yml.j2 < deploy.yml.diff
```
3. Modify your own inventory accordingly.
```yaml
# inventory/groupvars/k8s_masters.yml

dashboard: true
dashboard_version: v2.0.4
dashboard_nodeport: 31443
```

## Remove Dashboard
```bash
kubectl delete -f ~/.k8scluster/kubernetes-dashboard-account.yml
kubectl delete -f ~/.k8scluster/kubernetes-dashboard-deploy.yml
# Check results
kubectl get secret,sa,role,rolebinding,services,deployments --namespace=kubernetes-dashboard | grep dashboard
```
